m_inicial = int(input('Digite o valor da marcação inicial: '))
m_final = int(input('Digite o valor da marcação final: '))
qtd_comb = int(input('Digite a quantidade de combustível gasto: '))
valor = float(input('Digite o valor recebido no dia: '))
comb = 1.90

media = (m_final - m_inicial) / qtd_comb
lucro = valor - (qtd_comb * comb)

print(f'Média de consumo {round(media,2)}')
print(f'Lucro: {lucro}')

