def se_primo(n):
    i = 1
    cont = 0
    while i <= n:
        if n % i == 0:
            cont += 1
        i += 1
    if cont != 2:
        return 'O número não é primo'
    else:
        return 'O número é primo.'


num = int(input('Digite um número inteiro para verificar se é primo: '))

print(se_primo(num))